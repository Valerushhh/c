﻿using FactoryMethodGeneralizedConstructor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Mushroom.
    /// </summary>
    public abstract class Mushroom
    {
        /// <summary>
        /// Shroom id.
        /// </summary>
        private int _id;
        /// <summary>
        /// Shroom is edible or not.
        /// </summary>
        private bool _isEdible;
        /// <summary>
        /// Shroom is plucked or not.
        /// </summary>
        private bool _isPlucked;
        /// <summary>
        /// Shroom name.
        /// </summary>
        private String _shroomName;
        /// <summary>
        /// Shroom type.
        /// </summary>
        private ShroomTypes _shroomType;
        /// <summary>
        /// Prototype register.
        /// </summary>
        private static Hashtable _registry = new Hashtable();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parShroomType">Shroom type</param>
        /// <param name="parId">Shroom id</param>
        /// <param name="parShroomName">Shroom name</param>
        /// <param name="parIsEdible">Is shroom edible or not</param>
        /// <param name="parIsPlucked">Is shroom plucked or not</param>
        public Mushroom(ShroomTypes parShroomType, int parId, string parShroomName, bool parIsEdible, bool parIsPlucked)
        {
            _shroomType = parShroomType;
            _id = parId;
            _shroomName = parShroomName;
            _isEdible = parIsEdible;
            _isPlucked = parIsPlucked;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="parMushroom">Mushroom</param>
        protected Mushroom(Mushroom parMushroom)
        {
            this._shroomType = parMushroom._shroomType;
            this._id = parMushroom._id;
            this._shroomName = parMushroom._shroomName;
            this._isEdible = parMushroom._isEdible;
            this._isPlucked = parMushroom._isPlucked;
        }

        /// <summary>
        /// Get/sets shroom id.
        /// </summary>
        public int Id
        {
            get
            {
                return this._id;
            }

            set
            {
                this._id = value;
            }
        }

        /// <summary>
        /// Get/sets shroom suitability.
        /// </summary>
        public bool IsEdible
        {
            get
            {
                return this._isEdible;
            }

            set
            {
                this._isEdible = value;
            }
        }

        /// <summary>
        /// Get/sets shroom state.
        /// </summary>
        public bool IsPlucked
        {
            get
            {
                return this._isPlucked;
            }

            set
            {
                this._isPlucked = value;
            }
        }

        /// <summary>
        /// Get/sets shroom name.
        /// </summary>
        public string ShroomName
        {
            get
            {
                return this._shroomName;
            }

            set
            {
                this._shroomName = value;
            }
        }

        /// <summary>
        /// Creates mushroom
        /// </summary>
        /// <param name="parShroomType">Shroom type</param>
        /// <returns>Mushroom</returns>
        public static Mushroom CreateMushroom(ShroomTypes parShroomType)
        {
            Mushroom mushroom = null;
            if (_registry.Contains(parShroomType))
            {
                mushroom = (Mushroom)_registry[parShroomType];
                return mushroom.Clone();
            }
            return mushroom;
        }

        /// <summary>
        /// Adds prototype
        /// </summary>
        /// <param name="parShroomType">Shroom type</param>
        protected void AddPrototype(Mushroom parMushroom)
        {
            _registry[parMushroom._shroomType] = parMushroom;
        }

        /// <summary>
        /// Removes prototype
        /// </summary>
        /// <param name="parShroomType">Shroom type</param>
        protected void RemovePrototype(ShroomTypes parShroomType)
        {
            _registry.Remove(parShroomType);
        }

        /// <summary>
        /// Plucks the shroom.
        /// </summary>
        public void PluckShroom()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Clones shroom.
        /// </summary>
        /// <returns>Shroom</returns>
        public abstract Mushroom Clone();

        /// <summary>
        /// Info about shroom.
        /// </summary>
        /// <returns></returns>
        public abstract string Info();
    }
}