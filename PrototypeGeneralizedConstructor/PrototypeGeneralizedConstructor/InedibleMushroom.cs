﻿using FactoryMethodGeneralizedConstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Inedible mushroom.
    /// </summary>
    public class InedibleMushroom : Mushroom
    {
        /// <summary>
        /// Shroom is condimentable or not.
        /// </summary>
        private bool _isCondimentable;

        /// <summary>
        /// Constructor.
        /// </summary>
        public InedibleMushroom() : base(ShroomTypes.InedibleShroom, 1, "Inedible one", true, false)
        {
            _isCondimentable = true;

            base.AddPrototype(this);
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="parInedibleMushroom">Inedible mushroom</param>
        public InedibleMushroom(InedibleMushroom parInedibleMushroom) : base(parInedibleMushroom)
        {
            this._isCondimentable = parInedibleMushroom._isCondimentable;
        }

        /// <summary>
        /// Gets/sets shroom sutability for seasoning.
        /// </summary>
        public bool IsCondimentable
        {
            get
            {
                return this._isCondimentable;
            }

            set
            {
                this._isCondimentable = value;
            }
        }

        /// <summary>
        /// Uses shroom for season cooking.
        /// </summary>
        /// <param name="parSeason">Season the shroom will be used in</param>
        public void UseForSeason(string parSeason)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Clones shroom.
        /// </summary>
        /// <returns>Shroom</returns>
        public override Mushroom Clone()
        {
            InedibleMushroom inEdibleMushroom = new InedibleMushroom(this);
            return inEdibleMushroom;
        }

        /// <summary>
        /// Information about the shroom.
        /// </summary>
        /// <returns>Name of the shroom.</returns>
        public override string Info()
        {
            return String.Format("Shroom: {0}\r\n Edible: {1}\r\n Condimentable: {2}\r\n", this.ShroomName, this.IsPlucked, this.IsCondimentable);
        }
    }
}