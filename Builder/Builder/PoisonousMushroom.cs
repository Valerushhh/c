﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Poisonous mushroom.
    /// </summary>
    public class PoisonousMushroom : InedibleMushroom
    {
        /// <summary>
        /// Shroom is medically useful or not.
        /// </summary>
        private bool _isMedicallyUseful;

        /// <summary>
        /// Gets/sets shroom sutability for medicine.
        /// </summary>
        public bool IsMedicallyUseful
        {
            get
            {
                return IsMedicallyUseful;
            }

            set
            {
                IsMedicallyUseful = value;
            }
        }

        /// <summary>
        /// Uses shroom for medicine.
        /// </summary>
        /// <param name="outDrug">Drug the shroom will be used in</param>
        public void UseForMedicine(out string outDrug)
        {
            throw new System.NotImplementedException();
        }
    }
}