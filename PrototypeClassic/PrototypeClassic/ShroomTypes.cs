﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodGeneralizedConstructor
{
    /// <summary>
    /// Перечисление видов грибов
    /// </summary>
    public enum ShroomTypes
    {
        /// <summary>
        /// Съедобный гриб
        /// </summary>
        EdibleShroom,
        /// <summary>
        /// Несъедобный гриб
        /// </summary>
        InedibleShroom
    }
}
