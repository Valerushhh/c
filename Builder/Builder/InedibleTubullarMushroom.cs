﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс Несъедобный трубчатый гриб
    /// </summary>
    public class InedibleTubullarMushroom : lab_1.InedibleMushroom
    {
        /// <summary>
        /// Строковое представление
        /// </summary>
        /// <returns>Вид гриба</returns>
        public override string Info()
        {
            return "Inedible tubullar shroom";
        }
    }
}