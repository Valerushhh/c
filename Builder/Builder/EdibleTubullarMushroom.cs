﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс съедобный трубчатый гриб
    /// </summary>
    public class EdibleTubullarMushroom : lab_1.EdibleMushroom
    {
        /// <summary>
        /// Строковое представление
        /// </summary>
        /// <returns>Вид гриба</returns>
        public override string Info()
        {
            return "Edible tubullar shroom";
        }
    }
}