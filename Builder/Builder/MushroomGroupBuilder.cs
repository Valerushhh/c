﻿using lab_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс Строитель группы грибов
    /// </summary>
    public abstract class MushroomGroupBuilder
    {
        /// <summary>
        /// Список, содержащий грибы в группе
        /// </summary>
        private List<Mushroom> _group;

        /// <summary>
        /// Создает группу грибов 
        /// </summary>
        public void CreateGroup()
        {
            _group = new List<Mushroom>();
        }

        /// <summary>
        /// Добавляет съедобный гриб в группу
        /// </summary>
        public abstract void AddEdibleShroom();

        /// <summary>
        /// Добавляет несъедобный гриб в группу
        /// </summary>
        public abstract void AddInedibleShroom();

        /// <summary>
        /// Получает группу грибов
        /// </summary>
        public List<Mushroom> Group
        {
            get
            {
                return _group;
            }
        }
    }
}