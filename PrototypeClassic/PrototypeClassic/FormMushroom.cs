﻿using lab_1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototypeClassic
{
    /// <summary>
    /// Форма для грибов
    /// </summary>
    public partial class FormMushroom : Form
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public FormMushroom()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик по нажатию на кнопку "Add"
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event</param>
        private void buttonAddShroom_Click(object sender, EventArgs e)
        {
            Mushroom mushroom = (Mushroom)comboBoxShroomTypes.SelectedItem;
            textBoxMessage.Text = mushroom.Info();
        }

        /// <summary>
        /// Обработчик по нажатию на кнопку "Create factory"
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event</param>
        private void buttonCreateFactory_Click(object sender, EventArgs e)
        {
            if (textBoxEdibleShroomName.Text.Trim() != "" & textBoxInedibleShroomName.Text.Trim() != "")
            {
                EdibleMushroom edibleMushroom = new EdibleMushroom(textBoxEdibleShroomName.Text, checkBoxIsEdiblePlucked.Checked,
                    checkBoxIsEdibleEdible.Checked, checkBoxIsEdibleWormy.Checked,
                    checkBoxIsEdibleOld.Checked);
                InedibleMushroom inedibleMushroom = new InedibleMushroom(textBoxInedibleShroomName.Text, checkBoxIsInediblePlucked.Checked, 
                    checkBoxIsInedibleEdible.Checked, checkBoxIsInedibleCondimentable.Checked);

                PrototypeFactory prototypeFactory = new PrototypeFactory(edibleMushroom, inedibleMushroom);

                comboBoxShroomTypes.Items.Add(prototypeFactory.CreateEdibleMushroom());
                comboBoxShroomTypes.Items.Add(prototypeFactory.CreateInedibleMushroom());
                comboBoxShroomTypes.SelectedItem = comboBoxShroomTypes.Items[0];

                comboBoxShroomTypes.Enabled = true;
                buttonAddShroom.Enabled = true;
            }
        }
    }
}
