﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    // Класс Строитель группы трубчатых грибов
    /// </summary>
    public class TubullarMushroomGroupBuilder : MushroomGroupBuilder
    {
        /// <summary>
        /// Добавляет съедобный трубчатый гриб в группу
        /// </summary>
        public override void AddEdibleShroom()
        {
            Group.Add(new EdibleTubullarMushroom());
        }

        /// <summary>
        /// Добавляет несъедобный трубчатый гриб в группу
        /// </summary>
        public override void AddInedibleShroom()
        {
            Group.Add(new InedibleTubullarMushroom());
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        /// <returns>Название группы</returns>
        public override string ToString()
        {
            return "Tubullar mushroom group";
        }
    }
}