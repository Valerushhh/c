﻿using FactoryMethodGeneralizedConstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Edible mmushroom.
    /// </summary>
    public class EdibleMushroom : Mushroom
    {
        /// <summary>
        /// Shroom is wormy or not.
        /// </summary>
        private bool _isWormy;
        /// <summary>
        /// Shroom is old or not.
        /// </summary>
        private bool _isOld;
        /// <summary>
        /// Shroom value category.
        /// </summary>
        private string _shroomValueCategory;

        /// <summary>
        /// Constructor
        /// </summary>
        public EdibleMushroom() : base(ShroomTypes.EdibleShroom, 1, "Edible one", true, false)
        {
            _isWormy = false;
            _isOld = false;

            base.AddPrototype(this);
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="parEdibleMushroom">Edible mushroom</param>
        public EdibleMushroom(EdibleMushroom parEdibleMushroom) : base(parEdibleMushroom)
        {
            this._isWormy = parEdibleMushroom._isWormy;
            this._isOld = parEdibleMushroom._isOld;
        }

        /// <summary>
        /// Gets/sets shroom worminess.
        /// </summary>
        public bool IsWormy
        {
            get
            {
                return this._isWormy;
            }

            set
            {
                this._isWormy = value;
            }
        }

        /// <summary>
        /// Gets/sets shroom life expectancy.
        /// </summary>
        public bool IsOld
        {
            get
            {
                return this._isOld;
            }

            set
            {
                this._isOld = value;
            }
        }

        /// <summary>
        /// Gets/sets shroom value category.
        /// </summary>
        public string ShroomValueCategory
        {
            get
            {
                return this._shroomValueCategory;
            }

            set
            {
                this._shroomValueCategory = value;
            }
        }

        /// <summary>
        /// Uses shroom for season cooking.
        /// </summary>
        /// <param name="refDish">Dish the shroom will be used in</param>
        public void UseForCooking(ref string refDish)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Clones shroom.
        /// </summary>
        /// <returns>Shroom</returns>
        public override Mushroom Clone()
        {
            EdibleMushroom edibleMushroom = new EdibleMushroom(this);
            return edibleMushroom;
        }

        /// <summary>
        /// Information about the shroom.
        /// </summary>
        /// <returns>Name of the shroom.</returns>
        public override string Info()
        {
            return String.Format("Shroom: {0}\r\n Edible: {1}\r\n Wormy: {2}\r\n Old: {3}\r\n", this.ShroomName, this.IsPlucked, this.IsWormy, this.IsOld);
        }
    }
}