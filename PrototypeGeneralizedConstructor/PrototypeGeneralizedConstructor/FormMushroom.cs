﻿using FactoryMethodGeneralizedConstructor;
using lab_1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototypeGeneralizedConstructor
{
    /// <summary>
    /// Форма для грибов
    /// </summary>
    public partial class FormMushroom : Form
    {
        /// <summary>
        /// Список грибов
        /// </summary>
        private List<Mushroom> _MushroomList = new List<Mushroom>();

        /// <summary>
        /// Конструктор
        /// </summary>
        public FormMushroom()
        {
            InitializeComponent();

            comboBoxShroomTypes.Items.Add(ShroomTypes.EdibleShroom);
            comboBoxShroomTypes.Items.Add(ShroomTypes.InedibleShroom);
            comboBoxShroomTypes.SelectedItem = comboBoxShroomTypes.Items[0];
        }

        /// <summary>
        /// Обработчик по нажатию на кнопку "Add"
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event</param>
        private void buttonAddShroom_Click(object sender, EventArgs e)
        {
            EdibleMushroom edibleMushroom = new EdibleMushroom();
            InedibleMushroom inedibleMushroom = new InedibleMushroom();

            Mushroom mushroom = Mushroom.CreateMushroom((ShroomTypes)comboBoxShroomTypes.SelectedItem);
            textBoxMessage.Text = mushroom.Info();
        }
    }
}
