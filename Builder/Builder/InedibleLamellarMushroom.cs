﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс несъедобный пластинчатый гриб
    /// </summary>
    public class InedibleLamellarMushroom : lab_1.InedibleMushroom
    {
        /// <summary>
        /// Строковое представление
        /// </summary>
        /// <returns>Вид гриба</returns>
        public override string Info()
        {
            return "Inedible lamellar shroom";
        }
    }
}