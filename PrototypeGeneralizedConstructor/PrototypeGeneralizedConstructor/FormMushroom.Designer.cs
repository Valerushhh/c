﻿namespace PrototypeGeneralizedConstructor
{
    partial class FormMushroom
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAddShroom = new System.Windows.Forms.Label();
            this.comboBoxShroomTypes = new System.Windows.Forms.ComboBox();
            this.buttonAddShroom = new System.Windows.Forms.Button();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelAddShroom
            // 
            this.labelAddShroom.AutoSize = true;
            this.labelAddShroom.Location = new System.Drawing.Point(25, 49);
            this.labelAddShroom.Name = "labelAddShroom";
            this.labelAddShroom.Size = new System.Drawing.Size(66, 13);
            this.labelAddShroom.TabIndex = 0;
            this.labelAddShroom.Text = "Add shroom:";
            // 
            // comboBoxShroomTypes
            // 
            this.comboBoxShroomTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShroomTypes.FormattingEnabled = true;
            this.comboBoxShroomTypes.Location = new System.Drawing.Point(28, 88);
            this.comboBoxShroomTypes.Name = "comboBoxShroomTypes";
            this.comboBoxShroomTypes.Size = new System.Drawing.Size(156, 21);
            this.comboBoxShroomTypes.TabIndex = 1;
            // 
            // buttonAddShroom
            // 
            this.buttonAddShroom.Location = new System.Drawing.Point(209, 88);
            this.buttonAddShroom.Name = "buttonAddShroom";
            this.buttonAddShroom.Size = new System.Drawing.Size(60, 23);
            this.buttonAddShroom.TabIndex = 2;
            this.buttonAddShroom.Text = "Add";
            this.buttonAddShroom.UseVisualStyleBackColor = true;
            this.buttonAddShroom.Click += new System.EventHandler(this.buttonAddShroom_Click);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(28, 148);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(241, 154);
            this.textBoxMessage.TabIndex = 3;
            // 
            // FormMushroom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 350);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.buttonAddShroom);
            this.Controls.Add(this.comboBoxShroomTypes);
            this.Controls.Add(this.labelAddShroom);
            this.Name = "FormMushroom";
            this.Text = "FormMushroom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAddShroom;
        private System.Windows.Forms.ComboBox comboBoxShroomTypes;
        private System.Windows.Forms.Button buttonAddShroom;
        private System.Windows.Forms.TextBox textBoxMessage;
    }
}

