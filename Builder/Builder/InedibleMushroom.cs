﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Inedible mushroom.
    /// </summary>
    public class InedibleMushroom : Mushroom
    {
        /// <summary>
        /// Shroom is condimentable or not.
        /// </summary>
        private bool _isCondimentable;

        /// <summary>
        /// Gets/sets shroom sutability for seasoning.
        /// </summary>
        public bool IsCondimentable
        {
            get
            {
                return IsCondimentable;
            }

            set
            {
                IsCondimentable = value;
            }
        }

        /// <summary>
        /// Uses shroom for season cooking.
        /// </summary>
        /// <param name="parSeason">Season the shroom will be used in</param>
        public void UseForSeason(string parSeason)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Information about the shroom.
        /// </summary>
        /// <returns>Name of the shroom.</returns>
        public override string Info()
        {
            return "Inedible mushroom";
        }
    }
}