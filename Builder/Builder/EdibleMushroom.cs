﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Edible mmushroom.
    /// </summary>
    public class EdibleMushroom : Mushroom
    {
        /// <summary>
        /// Shroom is wormy or not.
        /// </summary>
        private bool _isWormy;
        /// <summary>
        /// Shroom is old or not.
        /// </summary>
        private bool _isOld;
        /// <summary>
        /// Shroom value category.
        /// </summary>
        private string _shroomValueCategory;

        /// <summary>
        /// Gets/sets shroom worminess.
        /// </summary>
        public bool IsWormy
        {
            get
            {
                return IsWormy;
            }

            set
            {
                IsWormy = value;
            }
        }

        /// <summary>
        /// Gets/sets shroom life expectancy.
        /// </summary>
        public int IsOld
        {
            get
            {
                return IsOld;
            }

            set
            {
                IsOld = value;
            }
        }

        /// <summary>
        /// Gets/sets shroom value category.
        /// </summary>
        public string ShroomValueCategory
        {
            get
            {
                return ShroomValueCategory;
            }

            set
            {
                ShroomValueCategory = value;
            }
        }

        /// <summary>
        /// Uses shroom for season cooking.
        /// </summary>
        /// <param name="refDish">Dish the shroom will be used in</param>
        public void UseForCooking(ref string refDish)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Information about the shroom.
        /// </summary>
        /// <returns>Name of the shroom.</returns>
        public override string Info()
        {
            return "Edible mushroom";
        }
    }
}