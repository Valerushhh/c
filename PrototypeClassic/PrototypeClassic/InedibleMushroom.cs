﻿using FactoryMethodGeneralizedConstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Inedible mushroom.
    /// </summary>
    public class InedibleMushroom : Mushroom
    {
        /// <summary>
        /// Shroom is condimentable or not.
        /// </summary>
        private bool _isCondimentable;


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parShroomName">Shroom name</param>
        /// <param name="parIsEdible">Is shroom edible or not</param>
        /// <param name="parIsPlucked">Is shroom plucked or not</param>
        /// <param name="parIsCondimentable">Is shroom condimentable or not</param>
        public InedibleMushroom(string parShroomName, bool parIsEdible, bool parIsPlucked, bool parIsCondimentable) : base(parShroomName, parIsEdible, parIsPlucked)
        {
            _isCondimentable = parIsCondimentable;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="parInedibleMushroom">Inedible mushroom</param>
        public InedibleMushroom(InedibleMushroom parInedibleMushroom) : base(parInedibleMushroom)
        {
            this._isCondimentable = parInedibleMushroom._isCondimentable;
        }

        /// <summary>
        /// Gets/sets shroom sutability for seasoning.
        /// </summary>
        public bool IsCondimentable
        {
            get
            {
                return this._isCondimentable;
            }

            set
            {
                this._isCondimentable = value;
            }
        }

        /// <summary>
        /// Uses shroom for season cooking.
        /// </summary>
        /// <param name="parSeason">Season the shroom will be used in</param>
        public void UseForSeason(string parSeason)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Clones shroom.
        /// </summary>
        /// <returns>Shroom</returns>
        public override Mushroom Clone()
        {
            InedibleMushroom inEdibleMushroom = new InedibleMushroom(this);
            return inEdibleMushroom;
        }

        /// <summary>
        /// Information about the shroom.
        /// </summary>
        /// <returns>Name of the shroom.</returns>
        public override string Info()
        {
            return String.Format("Shroom: {0}\r\n Edible: {1}\r\n Condimentable: {2}\r\n", this.ShroomName, this.IsPlucked, this.IsCondimentable);
        }

        /// <summary>
        /// String notation.
        /// </summary>
        /// <returns>Shroom type</returns>
        public override string ToString()
        {
            return "Inedible mushroom";
        }
    }
}