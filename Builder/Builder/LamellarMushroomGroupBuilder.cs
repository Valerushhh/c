﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс Строитель группы пластинчатых грибов
    /// </summary>
    public class LamellarMushroomGroupBuilder : MushroomGroupBuilder
    {
        /// <summary>
        /// Добавляет пластинчатый съедобный гриб в группу
        /// </summary>
        public override void AddEdibleShroom()
        {
            Group.Add(new EdibleLamellarMushroom());
        }

        /// <summary>
        /// Добавляет пластинчатый несъедобный гриб в группу
        /// </summary>
        public override void AddInedibleShroom()
        {
            Group.Add(new InedibleLamellarMushroom());
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        /// <returns>Название группы</returns>
        public override string ToString()
        {
            return "Lamellar mushroom group";
        }
    }
}