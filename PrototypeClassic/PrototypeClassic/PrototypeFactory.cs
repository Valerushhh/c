﻿using lab_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeClassic
{
    /// <summary>
    /// Класс Фабрика прототипа
    /// </summary>
    class PrototypeFactory
    {
        /// <summary>
        /// Съедобный гриб
        /// </summary>
        private EdibleMushroom _edibleMushroom;

        /// <summary>
        /// Несъедобный гриб
        /// </summary>
        private InedibleMushroom _inedibleMushroom;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="parEdibleMushroom">Съедобный гриб</param>
        /// <param name="parInedibleMusroom">Несъедобный гриб</param>
        public PrototypeFactory(EdibleMushroom parEdibleMushroom, InedibleMushroom parInedibleMusroom)
        {
            _edibleMushroom = parEdibleMushroom;
            _inedibleMushroom = parInedibleMusroom;
        }

        /// <summary>
        /// Создает съедобный гриб
        /// </summary>
        /// <returns>Клонированный съедобный гриб</returns>
        public Mushroom CreateEdibleMushroom()
        {
            return _edibleMushroom.Clone();
        }

        /// <summary>
        /// Создает несъедобный гриб
        /// </summary>
        /// <returns>Клонированный несъедобный гриб</returns>
        public Mushroom CreateInedibleMushroom()
        {
            return _inedibleMushroom.Clone();
        }
    }
}
