﻿using lab_1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    /// <summary>
    /// Форма для грибов
    /// </summary>
    public partial class FormMushroom : Form
    {
        /// <summary>
        /// Список грибов
        /// </summary>
        private List<Mushroom> _MushroomList = new List<Mushroom>();

        /// <summary>
        /// Конструктор
        /// </summary>
        public FormMushroom()
        {
            InitializeComponent();

            comboBoxShroomTypes.Items.Add(new TubullarMushroomGroupBuilder());
            comboBoxShroomTypes.Items.Add(new LamellarMushroomGroupBuilder());
            comboBoxShroomTypes.SelectedItem = comboBoxShroomTypes.Items[0];
        }

        /// <summary>
        /// Обновляет сообщение
        /// </summary>
        public void UpdateMessage()
        {
            textBoxMessage.Text = "";
            foreach (Mushroom shroom in _MushroomList)
            {
                textBoxMessage.Text += shroom.Info() + " added\r\n";
            }
        }

        /// <summary>
        /// Обработчик по нажатию на кнопку "Add"
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event</param>
        private void buttonAddShroom_Click(object sender, EventArgs e)
        {
            Director director = new Director();

            _MushroomList.AddRange(director.CreateShroomList((MushroomGroupBuilder)comboBoxShroomTypes.SelectedItem));
            UpdateMessage();
        }
    }
}
