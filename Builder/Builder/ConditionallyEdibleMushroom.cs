﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Conditionally edible mushroom.
    /// </summary>
    public class ConditionallyEdibleMushroom : EdibleMushroom
    {
        /// <summary>
        /// Shroom is cosmetology useful or not.
        /// </summary>
        private bool _isCosmetologyUseful;

        /// <summary>
        /// Gets/sets shroom sutability for cosmetology
        /// </summary>
        public bool IsCosmetologyUseful
        {
            get
            {
                return IsCosmetologyUseful;
            }

            set
            {
                IsCosmetologyUseful = value;
            }
        }

        /// <summary>
        /// Uses shroom for cosmetology.
        /// </summary>
        public void UseForCosmetology()
        {
            throw new System.NotImplementedException();
        }
    }
}