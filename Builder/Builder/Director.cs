﻿using lab_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс Директор
    /// </summary>
    public class Director
    {
        /// <summary>
        /// Создает список грибов
        /// </summary>
        /// <param name="parMushroomGroupBuilder">Строитель группы грибов</param>
        /// <returns>Список грибов</returns>
        public List<Mushroom> CreateShroomList(MushroomGroupBuilder parMushroomGroupBuilder)
        {
            parMushroomGroupBuilder.CreateGroup();
            parMushroomGroupBuilder.AddEdibleShroom();
            parMushroomGroupBuilder.AddInedibleShroom();
            return parMushroomGroupBuilder.Group;
        }
    }
}