﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab_1
{
    /// <summary>
    /// Mushroom.
    /// </summary>
    public abstract class Mushroom
    {
        /// <summary>
        /// Shroom id.
        /// </summary>
        private int _id;
        /// <summary>
        /// Shroom is edible or not.
        /// </summary>
        private bool _isEdible;
        /// <summary>
        /// Shroom is plucked or not.
        /// </summary>
        private bool _isPlucked;
        /// <summary>
        /// Shroom name.
        /// </summary>
        private String _shroomName;

        /// <summary>
        /// Get/sets shroom id.
        /// </summary>
        public int Id
        {
            get
            {
                return Id;
            }

            set
            {
                Id = value;
            }
        }

        /// <summary>
        /// Get/sets shroom suitability.
        /// </summary>
        public bool IsEdible
        {
            get
            {
                return IsEdible;
            }

            set
            {
                IsEdible = value;
            }
        }

        /// <summary>
        /// Get/sets shroom state.
        /// </summary>
        public bool IsPlucked
        {
            get
            {
                return IsPlucked;
            }

            set
            {
                IsPlucked = value;
            }
        }

        /// <summary>
        /// Get/sets shroom name.
        /// </summary>
        public string ShroomName
        {
            get
            {
                return ShroomName;
            }

            set
            {
                ShroomName = value;
            }
        }

        /// <summary>
        /// Plucks the shroom.
        /// </summary>
        public void PluckShroom()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Info about shroom.
        /// </summary>
        /// <returns></returns>
        public abstract string Info();
    }
}