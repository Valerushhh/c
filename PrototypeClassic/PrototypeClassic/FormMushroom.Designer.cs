﻿namespace PrototypeClassic
{
    partial class FormMushroom
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxShroomTypes = new System.Windows.Forms.ComboBox();
            this.buttonAddShroom = new System.Windows.Forms.Button();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.textBoxEdibleShroomName = new System.Windows.Forms.TextBox();
            this.labelEdibleShroomName = new System.Windows.Forms.Label();
            this.labelAddFactory = new System.Windows.Forms.Label();
            this.labelIsEdibleEdible = new System.Windows.Forms.Label();
            this.checkBoxIsEdibleEdible = new System.Windows.Forms.CheckBox();
            this.labelEdibleMushroom = new System.Windows.Forms.Label();
            this.labelInedibleMushroom = new System.Windows.Forms.Label();
            this.textBoxInedibleShroomName = new System.Windows.Forms.TextBox();
            this.labelInedibleShroomName = new System.Windows.Forms.Label();
            this.labelIsInedibleEdible = new System.Windows.Forms.Label();
            this.checkBoxIsInedibleEdible = new System.Windows.Forms.CheckBox();
            this.labelIsEdibleWormy = new System.Windows.Forms.Label();
            this.labelIsEdibleOld = new System.Windows.Forms.Label();
            this.checkBoxIsEdibleWormy = new System.Windows.Forms.CheckBox();
            this.checkBoxIsEdibleOld = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxIsInedibleCondimentable = new System.Windows.Forms.CheckBox();
            this.buttonCreateFactory = new System.Windows.Forms.Button();
            this.labelIsEdiblePlucked = new System.Windows.Forms.Label();
            this.checkBoxIsEdiblePlucked = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxIsInediblePlucked = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add shroom:";
            // 
            // comboBoxShroomTypes
            // 
            this.comboBoxShroomTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShroomTypes.Enabled = false;
            this.comboBoxShroomTypes.FormattingEnabled = true;
            this.comboBoxShroomTypes.Location = new System.Drawing.Point(23, 102);
            this.comboBoxShroomTypes.Name = "comboBoxShroomTypes";
            this.comboBoxShroomTypes.Size = new System.Drawing.Size(171, 21);
            this.comboBoxShroomTypes.TabIndex = 1;
            // 
            // buttonAddShroom
            // 
            this.buttonAddShroom.Enabled = false;
            this.buttonAddShroom.Location = new System.Drawing.Point(213, 102);
            this.buttonAddShroom.Name = "buttonAddShroom";
            this.buttonAddShroom.Size = new System.Drawing.Size(51, 23);
            this.buttonAddShroom.TabIndex = 2;
            this.buttonAddShroom.Text = "Add";
            this.buttonAddShroom.UseVisualStyleBackColor = true;
            this.buttonAddShroom.Click += new System.EventHandler(this.buttonAddShroom_Click);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(23, 151);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(241, 100);
            this.textBoxMessage.TabIndex = 3;
            // 
            // textBoxEdibleShroomName
            // 
            this.textBoxEdibleShroomName.Location = new System.Drawing.Point(403, 119);
            this.textBoxEdibleShroomName.Name = "textBoxEdibleShroomName";
            this.textBoxEdibleShroomName.Size = new System.Drawing.Size(100, 20);
            this.textBoxEdibleShroomName.TabIndex = 4;
            // 
            // labelEdibleShroomName
            // 
            this.labelEdibleShroomName.AutoSize = true;
            this.labelEdibleShroomName.Location = new System.Drawing.Point(302, 119);
            this.labelEdibleShroomName.Name = "labelEdibleShroomName";
            this.labelEdibleShroomName.Size = new System.Drawing.Size(75, 13);
            this.labelEdibleShroomName.TabIndex = 5;
            this.labelEdibleShroomName.Text = "Shroom name:";
            // 
            // labelAddFactory
            // 
            this.labelAddFactory.AutoSize = true;
            this.labelAddFactory.Location = new System.Drawing.Point(302, 61);
            this.labelAddFactory.Name = "labelAddFactory";
            this.labelAddFactory.Size = new System.Drawing.Size(64, 13);
            this.labelAddFactory.TabIndex = 6;
            this.labelAddFactory.Text = "Add factory:";
            // 
            // labelIsEdibleEdible
            // 
            this.labelIsEdibleEdible.AutoSize = true;
            this.labelIsEdibleEdible.Location = new System.Drawing.Point(335, 174);
            this.labelIsEdibleEdible.Name = "labelIsEdibleEdible";
            this.labelIsEdibleEdible.Size = new System.Drawing.Size(39, 13);
            this.labelIsEdibleEdible.TabIndex = 7;
            this.labelIsEdibleEdible.Text = "Edible:";
            // 
            // checkBoxIsEdibleEdible
            // 
            this.checkBoxIsEdibleEdible.AutoSize = true;
            this.checkBoxIsEdibleEdible.Location = new System.Drawing.Point(403, 173);
            this.checkBoxIsEdibleEdible.Name = "checkBoxIsEdibleEdible";
            this.checkBoxIsEdibleEdible.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsEdibleEdible.TabIndex = 8;
            this.checkBoxIsEdibleEdible.UseVisualStyleBackColor = true;
            // 
            // labelEdibleMushroom
            // 
            this.labelEdibleMushroom.AutoSize = true;
            this.labelEdibleMushroom.Location = new System.Drawing.Point(302, 88);
            this.labelEdibleMushroom.Name = "labelEdibleMushroom";
            this.labelEdibleMushroom.Size = new System.Drawing.Size(87, 13);
            this.labelEdibleMushroom.TabIndex = 9;
            this.labelEdibleMushroom.Text = "Edible mushroom";
            // 
            // labelInedibleMushroom
            // 
            this.labelInedibleMushroom.AutoSize = true;
            this.labelInedibleMushroom.Location = new System.Drawing.Point(530, 88);
            this.labelInedibleMushroom.Name = "labelInedibleMushroom";
            this.labelInedibleMushroom.Size = new System.Drawing.Size(95, 13);
            this.labelInedibleMushroom.TabIndex = 10;
            this.labelInedibleMushroom.Text = "Inedible mushroom";
            // 
            // textBoxInedibleShroomName
            // 
            this.textBoxInedibleShroomName.Location = new System.Drawing.Point(611, 116);
            this.textBoxInedibleShroomName.Name = "textBoxInedibleShroomName";
            this.textBoxInedibleShroomName.Size = new System.Drawing.Size(100, 20);
            this.textBoxInedibleShroomName.TabIndex = 11;
            // 
            // labelInedibleShroomName
            // 
            this.labelInedibleShroomName.AutoSize = true;
            this.labelInedibleShroomName.Location = new System.Drawing.Point(530, 119);
            this.labelInedibleShroomName.Name = "labelInedibleShroomName";
            this.labelInedibleShroomName.Size = new System.Drawing.Size(75, 13);
            this.labelInedibleShroomName.TabIndex = 12;
            this.labelInedibleShroomName.Text = "Shroom name:";
            // 
            // labelIsInedibleEdible
            // 
            this.labelIsInedibleEdible.AutoSize = true;
            this.labelIsInedibleEdible.Location = new System.Drawing.Point(566, 173);
            this.labelIsInedibleEdible.Name = "labelIsInedibleEdible";
            this.labelIsInedibleEdible.Size = new System.Drawing.Size(39, 13);
            this.labelIsInedibleEdible.TabIndex = 13;
            this.labelIsInedibleEdible.Text = "Edible:";
            // 
            // checkBoxIsInedibleEdible
            // 
            this.checkBoxIsInedibleEdible.AutoSize = true;
            this.checkBoxIsInedibleEdible.Location = new System.Drawing.Point(611, 172);
            this.checkBoxIsInedibleEdible.Name = "checkBoxIsInedibleEdible";
            this.checkBoxIsInedibleEdible.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsInedibleEdible.TabIndex = 14;
            this.checkBoxIsInedibleEdible.UseVisualStyleBackColor = true;
            // 
            // labelIsEdibleWormy
            // 
            this.labelIsEdibleWormy.AutoSize = true;
            this.labelIsEdibleWormy.Location = new System.Drawing.Point(331, 196);
            this.labelIsEdibleWormy.Name = "labelIsEdibleWormy";
            this.labelIsEdibleWormy.Size = new System.Drawing.Size(43, 13);
            this.labelIsEdibleWormy.TabIndex = 15;
            this.labelIsEdibleWormy.Text = "Wormy:";
            // 
            // labelIsEdibleOld
            // 
            this.labelIsEdibleOld.AutoSize = true;
            this.labelIsEdibleOld.Location = new System.Drawing.Point(348, 220);
            this.labelIsEdibleOld.Name = "labelIsEdibleOld";
            this.labelIsEdibleOld.Size = new System.Drawing.Size(26, 13);
            this.labelIsEdibleOld.TabIndex = 16;
            this.labelIsEdibleOld.Text = "Old:";
            // 
            // checkBoxIsEdibleWormy
            // 
            this.checkBoxIsEdibleWormy.AutoSize = true;
            this.checkBoxIsEdibleWormy.Location = new System.Drawing.Point(403, 195);
            this.checkBoxIsEdibleWormy.Name = "checkBoxIsEdibleWormy";
            this.checkBoxIsEdibleWormy.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsEdibleWormy.TabIndex = 17;
            this.checkBoxIsEdibleWormy.UseVisualStyleBackColor = true;
            // 
            // checkBoxIsEdibleOld
            // 
            this.checkBoxIsEdibleOld.AutoSize = true;
            this.checkBoxIsEdibleOld.Location = new System.Drawing.Point(403, 219);
            this.checkBoxIsEdibleOld.Name = "checkBoxIsEdibleOld";
            this.checkBoxIsEdibleOld.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsEdibleOld.TabIndex = 18;
            this.checkBoxIsEdibleOld.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(525, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Condimentable:";
            // 
            // checkBoxIsInedibleCondimentable
            // 
            this.checkBoxIsInedibleCondimentable.AutoSize = true;
            this.checkBoxIsInedibleCondimentable.Location = new System.Drawing.Point(611, 195);
            this.checkBoxIsInedibleCondimentable.Name = "checkBoxIsInedibleCondimentable";
            this.checkBoxIsInedibleCondimentable.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsInedibleCondimentable.TabIndex = 20;
            this.checkBoxIsInedibleCondimentable.UseVisualStyleBackColor = true;
            // 
            // buttonCreateFactory
            // 
            this.buttonCreateFactory.Location = new System.Drawing.Point(305, 273);
            this.buttonCreateFactory.Name = "buttonCreateFactory";
            this.buttonCreateFactory.Size = new System.Drawing.Size(102, 23);
            this.buttonCreateFactory.TabIndex = 21;
            this.buttonCreateFactory.Text = "Create factory";
            this.buttonCreateFactory.UseVisualStyleBackColor = true;
            this.buttonCreateFactory.Click += new System.EventHandler(this.buttonCreateFactory_Click);
            // 
            // labelIsEdiblePlucked
            // 
            this.labelIsEdiblePlucked.AutoSize = true;
            this.labelIsEdiblePlucked.Location = new System.Drawing.Point(325, 150);
            this.labelIsEdiblePlucked.Name = "labelIsEdiblePlucked";
            this.labelIsEdiblePlucked.Size = new System.Drawing.Size(49, 13);
            this.labelIsEdiblePlucked.TabIndex = 22;
            this.labelIsEdiblePlucked.Text = "Plucked:";
            // 
            // checkBoxIsEdiblePlucked
            // 
            this.checkBoxIsEdiblePlucked.AutoSize = true;
            this.checkBoxIsEdiblePlucked.Location = new System.Drawing.Point(403, 149);
            this.checkBoxIsEdiblePlucked.Name = "checkBoxIsEdiblePlucked";
            this.checkBoxIsEdiblePlucked.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsEdiblePlucked.TabIndex = 23;
            this.checkBoxIsEdiblePlucked.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(556, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Plucked:";
            // 
            // checkBoxIsInediblePlucked
            // 
            this.checkBoxIsInediblePlucked.AutoSize = true;
            this.checkBoxIsInediblePlucked.Location = new System.Drawing.Point(611, 148);
            this.checkBoxIsInediblePlucked.Name = "checkBoxIsInediblePlucked";
            this.checkBoxIsInediblePlucked.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsInediblePlucked.TabIndex = 25;
            this.checkBoxIsInediblePlucked.UseVisualStyleBackColor = true;
            // 
            // FormMushroom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 343);
            this.Controls.Add(this.checkBoxIsInediblePlucked);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBoxIsEdiblePlucked);
            this.Controls.Add(this.labelIsEdiblePlucked);
            this.Controls.Add(this.buttonCreateFactory);
            this.Controls.Add(this.checkBoxIsInedibleCondimentable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBoxIsEdibleOld);
            this.Controls.Add(this.checkBoxIsEdibleWormy);
            this.Controls.Add(this.labelIsEdibleOld);
            this.Controls.Add(this.labelIsEdibleWormy);
            this.Controls.Add(this.checkBoxIsInedibleEdible);
            this.Controls.Add(this.labelIsInedibleEdible);
            this.Controls.Add(this.labelInedibleShroomName);
            this.Controls.Add(this.textBoxInedibleShroomName);
            this.Controls.Add(this.labelInedibleMushroom);
            this.Controls.Add(this.labelEdibleMushroom);
            this.Controls.Add(this.checkBoxIsEdibleEdible);
            this.Controls.Add(this.labelIsEdibleEdible);
            this.Controls.Add(this.labelAddFactory);
            this.Controls.Add(this.labelEdibleShroomName);
            this.Controls.Add(this.textBoxEdibleShroomName);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.buttonAddShroom);
            this.Controls.Add(this.comboBoxShroomTypes);
            this.Controls.Add(this.label1);
            this.Name = "FormMushroom";
            this.Text = "FormMushroom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxShroomTypes;
        private System.Windows.Forms.Button buttonAddShroom;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.TextBox textBoxEdibleShroomName;
        private System.Windows.Forms.Label labelEdibleShroomName;
        private System.Windows.Forms.Label labelAddFactory;
        private System.Windows.Forms.Label labelIsEdibleEdible;
        private System.Windows.Forms.CheckBox checkBoxIsEdibleEdible;
        private System.Windows.Forms.Label labelEdibleMushroom;
        private System.Windows.Forms.Label labelInedibleMushroom;
        private System.Windows.Forms.TextBox textBoxInedibleShroomName;
        private System.Windows.Forms.Label labelInedibleShroomName;
        private System.Windows.Forms.Label labelIsInedibleEdible;
        private System.Windows.Forms.CheckBox checkBoxIsInedibleEdible;
        private System.Windows.Forms.Label labelIsEdibleWormy;
        private System.Windows.Forms.Label labelIsEdibleOld;
        private System.Windows.Forms.CheckBox checkBoxIsEdibleWormy;
        private System.Windows.Forms.CheckBox checkBoxIsEdibleOld;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxIsInedibleCondimentable;
        private System.Windows.Forms.Button buttonCreateFactory;
        private System.Windows.Forms.Label labelIsEdiblePlucked;
        private System.Windows.Forms.CheckBox checkBoxIsEdiblePlucked;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxIsInediblePlucked;
    }
}

