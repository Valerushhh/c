﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    /// <summary>
    /// Класс съедобный пластинчатый гриб
    /// </summary>
    public class EdibleLamellarMushroom : lab_1.EdibleMushroom
    {
        /// <summary>
        /// Строковое представление
        /// </summary>
        /// <returns>Вид гриба</returns>
        public override string Info()
        {
            return "Edible lamellar shroom";
        }
    }
}